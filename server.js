var express = require('express');


var app = express();

app.use(express.static(__dirname + '/js'));
app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/templates'));

app.set("views", __dirname + '/');
app.set("view engine", "ejs");

app.get('*', function(req, res) {
  res.render("index")
});

app.listen(3000, function(){
  console.log("Express server");
});
